package com.jeesite.modules.config;

import java.util.Hashtable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.sql.DataSource;

import bp.da.DataType;
import bp.difference.redis.RedisUtils;
import com.jeesite.common.config.Global;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.subject.support.DefaultSubjectContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import bp.difference.ContextHolderUtils;
import bp.difference.GvtvPropertyPlaceholderConfigurer;
import bp.difference.SystemConfig;

/**
 * JFlow配置
 * @author Bryce Han
 *
 */
@Configuration
@ComponentScan(basePackages = {"bp.difference"})
public class JFlowConfig {
	
	private static final Logger logger = LoggerFactory.getLogger(JFlowConfig.class);
	
	private static ApplicationContext applicationContext;
	
	@Autowired
	Environment env;
	
	/**
	 * 属性文件jflow.properties配置
	 * @return
	 */
	/*@Bean
	public static GvtvPropertyPlaceholderConfigurer propertyConfigurer() {
		GvtvPropertyPlaceholderConfigurer propertyConfigurer = new GvtvPropertyPlaceholderConfigurer();
		ResourceLoader resourceLoader = new DefaultResourceLoader();
		Resource resource = resourceLoader.getResource("classpath::config/jflow.properties");
		propertyConfigurer.setLocation(resource);
		return propertyConfigurer;
	}
	*/
	
	/**
	 * JFlow集成上下文工具类
	 * @param dataSource 数据源
	 * @return
	 */
	@Primary
	@Bean
	public ContextHolderUtils jflowContextHolderUtils(DataSource dataSource) {
		Hashtable<String, Object> prop = SystemConfig.getCS_AppSettings();
		prop.put("AppCenterDBType", Global.getProperty("jdbc.type"));
		prop.put("AppCenterDBDatabase", Global.getProperty("jdbc.schema"));
		prop.put("AppCenterDSN", Global.getProperty("jdbc.url"));
		prop.put("JflowUser", Global.getProperty("jdbc.username"));
		prop.put("JflowPassword", Global.getProperty("jdbc.password"));
		prop.put("JflowTestSql", Global.getProperty("jdbc.testSql"));
		prop.put("RedisIsEnable",env.getProperty("spring.data.redis.repositories.enabled").equals("true")?1:0);
		ContextHolderUtils bean = new ContextHolderUtils();
		bean.setUserNoSessionKey(DefaultSubjectContext.PRINCIPALS_SESSION_KEY);
		bean.setDataSource(dataSource);
		if(applicationContext != null) {
			bean.setApplicationContext(applicationContext);
		}
		
		return bean;
	}
	/**
	 * JFlow集成上下文工具类
	 * @param redisUtils  redis
	 * @return
	 */
	@Bean
	@ConditionalOnProperty(name="spring.data.redis.repositories.enabled", havingValue="true", matchIfMissing=false)
	public ContextHolderUtils jflowRedisContextHolderUtils(RedisUtils redisUtils) {
		ContextHolderUtils contextHolderUtils = new ContextHolderUtils();
		contextHolderUtils.setRedisUtils(redisUtils);
		if(applicationContext != null) {
			contextHolderUtils.setApplicationContext(applicationContext);
		}
		return contextHolderUtils;
	}

	public static ApplicationContext getApplicationContext() {
		return applicationContext;
	}

	public static void setApplicationContext(ApplicationContext applicationContext) {
		JFlowConfig.applicationContext = applicationContext;
	}

}
