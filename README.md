﻿#### 产品说明
- 在右上角点: watch, star, fork 支持我们.
- 谢谢,技术交流群：819789189
- JeeSite-JFlow 是 JeeSite 开发平台与 JFlow 流程开发平台集成的版本。
- 您即可以使用 JeeSite 的敏捷性开发，也可以使用国内著名开源工作流\表单引擎 JFlow。
- 我们已经把 JeeSite 与 JFlow 的组织结构深度集成在一起。
- JeeSite 与 JFlow 两者珠联璧合，是您开发的利器，不二的选择.
- JeeSite 官网：<http://jeesite.com>   JFlow官网：<http://ccflow.org>.
- JFlow服务电话(微信): 18660153393,QQ:793719823
- 网站-演示: http://jeesite.jflow.cn

#### 客户案例
- 部分客户案例，排名不分先后.
![输入图片说明](docs/Picture/Case.png)

#### 应用截图
- 系统主页1
![输入图片说明](docs/Picture/00.Homejeesite.png)
- 系统主页2
![输入图片说明](docs/Picture/00.HomeJFlow.png)
- 节点属性
![输入图片说明](docs/Picture/30.AttrNode.png)
- 流程属性
![输入图片说明](docs/Picture/31.AttrFlow.png)
- 流程设计
 ![输入图片说明](https://foruda.gitee.com/images/1674176053502637478/3d837ea9_358162.png)
- 表单设计1
![输入图片说明](docs/Picture/20.FrmD.png)
- 表单设计2
![输入图片说明](docs/Picture/21.FrmD.png)


#### 安装教程

1. 环境准备：`JDK 1.8`、`Maven 3.3+`、`MySQL 5.7 or 8.0`
2. 下载源码：<https://gitee.com/thinkgem/jeesite4-jflow>
3. 将 `/JFlow/jflow-parent`、`/JFlow/jflow-core`、`/modules/jflow`、`/root`、`/web` 五个项目使用 Maven 方式导入到Eclipse，若是 IDEA 直接导入 `/root/pom.xml` 即可。
4. 打开 `/web/src/main/resources/config/application.yml` 文件，配置JDBC连接
5. 执行 `/root/bin/package.bat` 将依赖项目安装到本地Maven仓库。
6. 手动导入 `/docs/数据库脚本文件/mysql/jeesite_jflowForMySql*.sql` 脚本到数据库，其它数据库也可以执行 `/web/bin/init-data.bat(sh)` 初始化 mysql、oracle、sqlserver 数据库
7. Eclipse 或 IDEA 导入或打开 `web` 项目，直接运行 Application.java 的 main 方法即可启动服务。
8. 浏览器访问：<http://127.0.0.1:8980/js/>  账号 system 密码 admin

#### 安装常见问题

1. 初次访问流程设计器可能会报一些错误，JFlow有自动修复表机制，多访问几次，就不报错了。
2. 遇见流程设计器访问提示404，说明你的webapp目录没有部署成功，请clean重新编译，运行Application.java的main方法启动，
   若还不行，请尝试 <https://gitee.com/thinkgem/jeesite4-jflow/tree/v5.2/docs/IDEA开发环境下404问题.docx>
3. 提示 `Could not initialize class java.sql.SQLException`，遇见这个错误设置 MySql 的连接池参数 useSSL=false
4. 提示 `port_emp` 表不存在或无权限访问，请先检查数据库中 `port_emp` 视图是否存在，如果不存在可执行下 <https://gitee.com/thinkgem/jeesite4-jflow/blob/v5.2/web/db/mysql/jflow_view.sql> 脚本（忽略错误）重建Port视图；
5. 如果存在，检查 `application.yml` 的 `jdbc.schema` 参数是否设置（一般设置为数据库名）。
6. 提示 `Table 'xxx.xxxtrack' doesn't exist` 是因为该演示流程数据表不全，你进入流程设计器找到该编号的流程重新保存一次即可
7. 其它安装常见问题：<http://jeesite.com/?t=284210>

#### 其它链接

1. JFlow：https://gitee.com/opencc/JFlow
2. JeeSite 1.2：https://gitee.com/thinkgem/jeesite
3. JeeSite 4.x：https://gitee.com/thinkgem/jeesite4
4. JeeSite Vue 5.x：https://gitee.com/thinkgem/jeesite-vue
5. JeeSite Spring Cloud：https://gitee.com/thinkgem/jeesite4-cloud
6. JeeSite 手机端：https://gitee.com/thinkgem/jeesite4-uniapp
7. asp.net 版本的 ccflow：https://gitee.com/opencc/ccflow
8. 驰骋 BPM 官方网站：http://ccflow.org
9. JeeSite 官方网站：http://jeesite.com


#### 驰骋BPM介绍

-  驰骋工作流引擎研发与2003年，具有.net与java两个版本，这两个版本代码结构，数据库结构，设计思想，功能组成， 操作手册，完全相同。 导入导出的流程模版，表单模版两个版本完全通用。
-  我们把驰骋工作流程引擎简称ccbpm, CCFlow是.net版本的简称，JFlow是java版本的简称，我们未来将要发布python版本的PFlow,敬请关注.
-  十多年来，我们一直践行自己的诺言，真心服务中国IT产业，努力提高产品质量，成为了国内知名的老牌工作流引擎。
-  ccbpm作简单、概念通俗易懂、操作手册完善（计:14万操作手册说明书）、代码注释完整、案例丰富翔实、单元测试完整。
-  ccbpm包含表单引擎与流程引擎两大部分，并且两块完美结合，流程引擎对表单引擎的操纵，协同高效工作, 完成了很多国内生产审批模式下的流程设计,
-  ccbpm的流程与表单界面可视化的设计，可配置程度高，采用结构化的表单模版设计,集中解析模式的设计. 适应于中国国情的多种场景的需要、配置所见即所得、低代码、高配置.
-  ccbpm 在国内拥有最广泛的研究群体与应用客户群，是大型集团企业IT部门、软件公司、研究院、高校研究与应用的产品。
-  ccbpm不仅仅能够满足中小企业的需要，也能满足通信级用户的应用，先后在西门子、海南航空、中船、陕汽重卡、山东省国土资源厅、华电国际、江苏山东吉林测绘院、厦门证券、天业集团、天津港等国内外大型企业政府单位使用。
-  ccbpm可以独立运行，也可以作为中间件嵌入您的开发架构,还可以作为服务的模式支持对外发布.
-  ccbpm 既有配置类型的开发适用于业务人员，IT维护人员， 也有面向程序员的高级引擎API开发,满足不同层次的流程设计人员需要.
-  ccbpm 支持 oracle, sqlserver, mysql 数据库.
-  流程引擎设计支持所见即所得的设计：节点设计、表单设计、单据设计、报表定义设计、以及用户菜单设计。
-  流程模式简洁，只有4种容易理解：线性流程、同表单分合流、异表单分合流、父子流程，没有复杂的概念。
-  配置参数丰富，支持流程的基础功能：前进、后退、转向、转发、撤销、抄送、挂起、草稿、任务池共享，也支持高级功能取回审批、项目组、外部用户等等。
