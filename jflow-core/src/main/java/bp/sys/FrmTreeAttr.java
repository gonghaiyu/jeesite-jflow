package bp.sys;

import bp.da.*;
import bp.en.*;
import bp.port.*;
import bp.sys.*;
import bp.*;
import java.util.*;

/** 
 属性
*/
public class FrmTreeAttr extends EntityTreeAttr
{
	/** 
	 数据源
	*/
	public static final String DBSrc = "DBSrc";
	/** 
	 组织编号
	*/
	public static final String OrgNo = "OrgNo";
}